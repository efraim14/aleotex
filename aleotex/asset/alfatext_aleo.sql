-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 01, 2017 at 12:52 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alfatext_aleo`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(6, 'Jackets', '2016-03-30 23:36:16', '2016-03-30 23:36:16'),
(7, 'Shirts', '2016-03-31 08:19:57', '2016-03-31 08:19:57'),
(8, 'Polo Shirts', '2016-04-10 23:50:26', '2016-04-10 23:50:26'),
(9, 'T-Shirts', '2016-04-10 23:54:22', '2016-04-10 23:54:22');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `client_name`, `picture`, `created_at`, `updated_at`) VALUES
(5, 'FTUI', '5.png', '2016-03-30 23:37:41', '2016-03-30 23:37:41');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_category_id_foreign` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `category_id`, `item_name`, `picture`, `created_at`, `updated_at`) VALUES
(10, 6, 'Jeans', '10.jpg', '2016-03-30 23:41:55', '2016-03-30 23:41:55'),
(11, 6, 'Lotto Sweater', '11.jpg', '2016-03-31 08:10:06', '2016-03-31 08:10:06'),
(12, 6, 'Diadora Sweater', '12.jpg', '2016-03-31 08:10:34', '2016-03-31 08:10:34'),
(13, 6, 'Fleece', '13.jpg', '2016-03-31 08:11:00', '2016-03-31 08:11:00'),
(14, 6, 'Parachute', '14.jpg', '2016-03-31 08:15:10', '2016-03-31 08:15:10'),
(15, 6, 'Twist', '15.jpg', '2016-03-31 08:15:30', '2016-03-31 08:15:30'),
(16, 6, 'Drill', '16.jpg', '2016-03-31 08:16:39', '2016-03-31 08:16:39'),
(17, 6, 'Sweeding Canvas', '17.jpg', '2016-03-31 08:16:57', '2016-03-31 08:16:57'),
(18, 6, 'Micro', '18.jpg', '2016-03-31 08:17:34', '2016-03-31 08:17:34'),
(19, 6, 'Micro Baseball', '19.jpg', '2016-03-31 08:17:53', '2016-03-31 08:17:53'),
(20, 6, 'Taslan', '20.jpg', '2016-03-31 08:18:19', '2016-03-31 08:18:19'),
(21, 6, 'Wearpack', '21.jpg', '2016-03-31 08:18:54', '2016-03-31 08:18:54'),
(24, 7, 'Oxford', '24.jpg', '2016-03-31 08:20:47', '2016-03-31 08:20:47'),
(25, 7, 'Drill Shirt', '25.jpg', '2016-03-31 08:21:08', '2016-03-31 08:21:08'),
(26, 7, 'Twist Shirt', '26.png', '2016-03-31 08:21:31', '2016-03-31 08:21:31'),
(27, 7, 'Sandwash Shirt', '27.jpg', '2016-03-31 08:23:17', '2016-03-31 08:23:17'),
(30, 8, 'Polo Lacoste PE', '30.jpg', '2016-04-10 23:50:26', '2016-04-10 23:50:26'),
(31, 8, 'Polo Cotton Combed', '31.jpg', '2016-04-10 23:51:23', '2016-04-10 23:51:23'),
(32, 8, 'Polo Pique', '32.jpg', '2016-04-10 23:52:38', '2016-04-10 23:52:38'),
(33, 9, 'Cotton Combed 24s', '33.png', '2016-04-10 23:54:22', '2016-04-10 23:54:22'),
(34, 9, 'Cotton Cardeed', '34.jpg', '2016-04-10 23:59:28', '2016-04-10 23:59:28'),
(35, 9, 'PE', '35.jpg', '2016-04-11 00:01:14', '2016-04-11 00:01:14'),
(36, 9, 'Double PE', '36.jpg', '2016-04-11 00:03:53', '2016-04-11 00:03:53'),
(37, 9, 'Cotton Combed 30s', '37.jpg', '2016-04-11 00:09:28', '2016-04-11 00:09:28'),
(38, 9, 'Cotton Combed 20s', '38.jpg', '2016-04-11 00:09:46', '2016-04-11 00:09:46'),
(39, 9, 'Double Cotton Premium', '39.jpg', '2016-04-11 00:10:42', '2016-04-11 00:10:42');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`material_id`, `material_name`, `picture`, `created_at`, `updated_at`) VALUES
(8, 'Cotton', '8.jpg', '2016-03-30 23:35:17', '2016-03-30 23:35:17'),
(9, 'Canvas', '9.jpg', '2016-03-31 08:26:58', '2016-03-31 08:26:58'),
(10, 'Jeans', '10.jpg', '2016-03-31 08:27:05', '2016-03-31 08:27:54'),
(11, 'Taslan', '11.jpg', '2016-03-31 08:27:12', '2016-03-31 08:27:12'),
(12, 'Twist', '12.jpg', '2016-04-10 23:12:29', '2016-04-10 23:12:29'),
(13, 'Ripstop', '13.jpg', '2016-04-10 23:30:28', '2016-04-10 23:30:28'),
(14, 'Fleece', '14.jpg', '2016-04-10 23:31:10', '2016-04-10 23:31:10'),
(15, 'Pique', '15.jpg', '2016-04-10 23:31:50', '2016-04-10 23:31:50'),
(16, 'Oxford', '16.jpg', '2016-04-10 23:32:26', '2016-04-10 23:32:27'),
(17, 'Sandwash', '17.jpg', '2016-04-10 23:32:56', '2016-04-10 23:32:56'),
(18, 'Drill', '18.jpg', '2016-04-10 23:33:27', '2016-04-10 23:33:27'),
(19, 'Parachute', '19.jpg', '2016-04-10 23:33:49', '2016-04-10 23:33:49'),
(20, 'Micro', '20.jpg', '2016-04-10 23:34:15', '2016-04-10 23:34:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_27_155703_create_material_table', 1),
('2016_03_27_155802_create_client_table', 1),
('2016_03_27_155912_create_question_table', 1),
('2016_03_27_160409_create_category_table', 1),
('2016_03_27_160919_create_item_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'efraim', '$2y$10$rIrUqGzbiB0O5WbAU7Jua.jtAJ10XDO9lM0Rxa7d5B2VWhtBViKou', 'RjyZlveHXjXXybL6TQyrNb2Pymlzk7PPalP7rfrhkRx31zLe2xE92A2xDMqu', NULL, '2016-03-30 20:08:45'),
(2, 'davidjendra', '$2y$10$XyFTlngbKE4.WAhuvbHfK.AAd8Z.fRE7Cv/zr3lV5ry.cpl9Dfkeu', 'DOdX7x1fAS0JVmyYOsUVnfmJh9q3Y2Zt39T4HL4PJGwB1udIDlisOkkKTNt0', '2016-03-30 20:08:35', '2016-03-30 23:47:22');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
