<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::post('/contact','HomeController@postContact');

//Authentication
Route::get('/login', 'UserController@getLogin');
Route::post('/login', 'UserController@postLogin');
Route::get('/logout', 'UserController@getLogout');

//user
Route::resource('user', 'UserController');

//category
Route::resource('category', 'CategoryController');

//material
Route::get('/material/editmode','MaterialController@editMode');
Route::resource('material', 'MaterialController');

//client
Route::resource('client', 'ClientController');

//client
Route::resource('product', 'ItemController');