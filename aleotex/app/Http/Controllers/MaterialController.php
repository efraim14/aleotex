<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Material;
use Input;
use Storage;
use File;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'index',
        ]]);
    }

    public function editMode()
    {
        $materials = Material::all();
        return view('material.editmode')->with('materials',$materials);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::all();
        return view('material.index')->with('materials',$materials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('material.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'material_name' => 'required|unique:material',
        'file'          => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        $input = $request->all();

        $file = Input::file('file');
		
		$material = new Material;
		
		$material->save();

        $filename = $material->material_id.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/img_material';

        $upload_success = $file->move($destinationPath, $filename);

        $input['picture'] = $filename;

        $material->fill($input)->save();
        flash()->success('Material successfully created!');
        return redirect()->route('material.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Material::findOrFail($id);
        return view('material.edit')->with('material',$material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'material_name' => 'required|unique:material,material_name,'.$id.',material_id',
        'file'          => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $input = $request->all();

        if ($request->hasFile('file')) {

        $file = Input::file('file');

        $filename = $id.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/img_material';

        $upload_success = $file->move($destinationPath, $filename);

        $input['picture'] = $filename;
        }

        $material = Material::findOrFail($id);
        $material->fill($input)->save();
        flash()->success('Material successfully updated!');
        return redirect()->route('material.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::findOrFail($id);
        File::delete(public_path().'/img_material/'.$material->picture);
        $material->delete();
        flash()->success('Material successfully deleted!');
        return redirect()->route('material.index');
    }

}
