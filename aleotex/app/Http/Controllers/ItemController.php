<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use App\Item;
use App\Category;
use File;


class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'index',
        ]]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('item.index')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('item.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'item_name' => 'required|unique:item',
		'category_id'	=> 'required',
        'file'  => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        $input = $request->all();

        if($input['category_id']=="new")
        {
            $this->validate($request, [
            'category_name' => 'required|unique:category',
            ]);

        }

        $file = Input::file('file');
		
		if($input['category_id']=="new")
        {
            $category = new Category;
            $category->category_name = $input['category_name'];
            $category->save();
            $input['category_id'] = $category->category_id;

        }
		
		$item = new Item;
		
		$item->category_id = $input['category_id'];
		
		$item->save();
		
        $filename = $item->item_id.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/img_item';

        $upload_success = $file->move($destinationPath, $filename);

        $input['picture'] = $filename;

        $item->fill($input)->save();
        flash()->success('Item successfully created!');
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $item = Item::findOrFail($id);
        $data = array('categories'=>$categories,'item'=>$item);
        return view('item.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'item_name' => 'required|unique:item,item_name,'.$id.',item_id',
		'category_id'	=> 'required',
        'file'  => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $input = $request->all();
        if ($request->hasFile('file')) {

        $file = Input::file('file');

        $filename = $id.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/img_item';

        $upload_success = $file->move($destinationPath, $filename);

        $input['picture'] = $filename;
        }

        $item = Item::findOrFail($id);
        $item->fill($input)->save();

        flash()->success('Item successfully updated!');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        File::delete(public_path().'/img_item/'.$item->picture);
        $item->delete();
        flash()->success('Item successfully deleted!');
        return redirect()->route('product.index');
    }
}
