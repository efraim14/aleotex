<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use App\Client;
use File;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'index',
        ]]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('client.index')->with('clients',$clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'client_name' => 'required|unique:client',
        'file'   => 'required|mimes:jpeg,bmp,png,jpg',
        ]);


        $input = $request->all();

        $file = Input::file('file');
		
		$client = new Client;
		
		$client->save();
		
        $filename = $client->client_id.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/img_client';

        $upload_success = $file->move($destinationPath, $filename);

        $input['picture'] = $filename;

		$client->client_name = $input['client_name'];
		$client->picture = $input['picture'];
		$client->save();
        flash()->success('Client successfully created!');
        return redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('client.edit')->with('client',$client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'client_name' => 'required|unique:client,client_name,'.$id.',client_id',
        'file'          => 'mimes:jpeg,bmp,png,jpg',
        ]);

        $input = $request->all();

        if ($request->hasFile('file'))
		{

        $file = Input::file('file');

        $filename = $id.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/img_client';

        $upload_success = $file->move($destinationPath, $filename);

        $input['picture'] = $filename;
        }

        $client = Client::findOrFail($id);
        $client->fill($input)->save();
        flash()->success('Client successfully updated!');
        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        File::delete(public_path().'/img_client/'.$client->picture);
        $client->delete();
        flash()->success('Client successfully deleted!');
        return redirect()->route('client.index');
    }
}
