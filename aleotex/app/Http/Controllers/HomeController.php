<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Material;
use Mail;
use Input;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::orderByRaw("RAND()")->take(3)->get();
        return view('home')->with('materials',$materials);
    }
	
	public function postContact()
	{
	    $data = Input::all();
	    $rules = array(
		  	'name' => 'required',
		  	'email' => 'required|email',
			'g-recaptcha-response' => 'required|captcha',
			'message' => 'required',
		);
		$validator = Validator::make($data, $rules);
		if ($validator->fails())
		{
		    return redirect()->back()->withInput()->withErrors($validator);
		}
		else
		{
		        //Send email using Laravel send function
                Mail::send('emails.contact', $data, function($message) use ($data)
                {
				//email 'From' field: Get users email add and name
				$message->from($data['email'] , $data['name']);
				//email 'To' field: cahnge this to emails that you want to be notified.                    
				$message->to('alfatextilesjakarta@gmail.com', 'alfatextiles')->cc('alfatextilesjakarta@gmail.com')->subject('contact request');
 
                });
				return redirect()->back();
		}
	}
}
