<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table='item';
	protected $primaryKey='item_id';
	protected $fillable = ['item_name','category_id','picture'];

	public function category()
    {
        return $this->belongsTo('App\Category','category_id','category_id');
    }
}
