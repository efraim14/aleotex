<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='category';
	protected $primaryKey='category_id';
	protected $fillable = ['category_name'];

	public function items()
    {
        return $this->hasMany('App\Item','category_id','category_id');
    }
}
