<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table='material';
	protected $primaryKey='material_id';
	protected $fillable = ['material_name', 'picture'];
}
