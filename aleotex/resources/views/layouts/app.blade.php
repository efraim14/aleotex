<!DOCTYPE HTML>
<html>
<head>
<title>ALFATEX - @yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Aleotex textile convection garment" />
<!-- css files -->
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ URL::asset('css/team.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ URL::asset('css/cobox.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ URL::asset('css/navigation.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
<!-- /css files -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- /fonts -->
<!-- js files -->
<script src="{{ URL::asset('js/SmoothScroll.min.js') }}"></script>
<script src="{{ URL::asset('js/modernizr.js') }}"></script> <!-- Modernizr -->
<!-- /js files -->
</head>
<body data-spy="scroll" data-offset="0" data-target="#navigation">
<!-- Navigation -->
<section class="cd-section">
		<button class="cd-bouncy-nav-trigger" href="#0">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
	</section>
	<div class="cd-bouncy-nav-modal">
		<nav>
			<ul class="cd-bouncy-nav">
				<li><a href="{{ url('../#home') }}">Home</a></li>
				<li><a href="{{ route('product.index') }}">Products</a></li>
				<li><a href="{{ route('material.index') }}">Raw Materials</a></li>
				<li><a href="{{ route('client.index') }}">Our Clients</a></li>
				@if(Auth::check())
				<li><a href="logout">Log Out</a></li>
				@endif
				<li><a href="{{ url('../#contact') }}">Contact Us</a></li>
			</ul>
		</nav>
		<a href="#0" class="cd-close">Close modal</a>
	</div>
<!-- /Navigation -->

@yield('content')

<div id="copyright">
	<div class="container">
		<p>Copyright 2016 Alfatex. All Rights Reserved | Design by <a href="mailto:efraim94@yahoo.com"><u>YT</u></a> | <a href="http://w3layouts.com">W3L</a></p>
	</div>
</div>
<a href="#0" class="cd-top">Top</a>

<!-- js files -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/smoothscroll.js') }}"></script>
<script>
	$('.carousel').carousel({
	interval: 3500
	})
</script>
<script src="{{ URL::asset('js/navigation.js') }}"></script>
<!-- js for gallery -->	
<script src="{{ URL::asset('js/cobox.js') }}" type="text/javascript"></script>
<!-- /js for gallery -->
<!-- js for back to top -->
<script src="{{ URL::asset('js/backtotop.js') }}"></script>
<!-- /js for back to top -->		
<!-- /js files -->
<script src='https://www.google.com/recaptcha/api.js'></script>
@stack('scripts')

</body>
</html>
