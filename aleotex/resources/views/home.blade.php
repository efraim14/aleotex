@extends('layouts.app')

@section('title')
Home
@endsection

@section('content')
<!-- Banner -->	
<section id="home" name="home"></section>
<div id="headerwrap">	
	<div class="container">
		<div class="logo">
			<img src="{{ URL::asset('images/logo.png') }}" style="width: 200px" />
		</div>
		<div class="banner-info">
			<h1>ALFATEX</h1>
			<h3>textile | convection | garment</h3>
	    </div>
	</div> <!--/ .container -->
</div><!--/ #headerwrap -->
<!-- /Banner -->
<!-- About -->
<section id="about" name="desc"></section>
<!-- INTRO WRAP -->
<div id="intro">
	<h3 class="text-center">Local Raw Materials</h3>
	<div class="container">
		<div class="row centered">
			@foreach($materials as $material)
			<div class="col-sm-4 intro-info">
				<img src="{{ URL::asset('img_material') }}/{{ $material->picture }}" alt="" style="width: 200px; height: 200px">
				<h4>{{ strtoupper($material->material_name) }}</h4>
			</div>
			@endforeach
		</div>
	</div> <!--/ .container -->
</div><!--/ #introwrap -->
<!-- About -->
<!-- Contact Us -->
<section id="contact" name="services"></section>
<div id="features">
	<h3 class="text-center">Contact Us</h3>
	<div class="container">
		<div class="row">
			<div class="col-md-7 contact-info">
			<h4>Drop Us A Line?</h4>
				<!-- ACCORDION -->
				<div class="accordion ac" id="accordion2">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Click <u>Here</u></a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								<form action="{{ url('/contact') }}" method="POST">
								{!! csrf_field() !!}
									<p class="your-para">Your Name :</p>
									<input type="text" value="{{ old('name') }}" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='';}" name="name">
									<p class="your-para">Your Mail :</p>
									<input type="text" value="{{ old('email') }}" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='';}" name="email">
									<p class="your-para">Your Message :</p>
									<textarea cols="77" rows="4" onfocus="this.value='';" onblur="if (this.value == '') {this.value = '';}" name="message"></textarea>
									<p class="your-para">Human Verification</p>
									{!! app('captcha')->display(); !!}
									<div class="send">
										<input type="submit" value="Send" >
									</div>
								</form>
							</div><!-- /accordion-inner -->
						</div><!-- /collapse -->
					</div><!-- /accordion-group -->
		        </div><!-- Accordion -->
			</div>
			
			<div class="col-md-5">
			<h4>Address</h4>
			<address>
				<strong>Alfatex</strong><br>
				Jembatan Niaga 2 lt.3 no. 52, Mangga Dua<br>
				Jakarta, Indonesia<br>
				<abbr title="Phone">P:</abbr> +(62)82-1136-10797
			</address>
			<address>
				<strong>Mail To Us</strong><br>
				<a href="mailto:alfatextilesjakarta@gmail.com">alfatextilesjakarta@gmail.com</a>
			</address>
			</div>
		</div>
	</div><!--/ .container -->
</div><!--/ #features -->
<!-- /Contact Us -->

@endsection
