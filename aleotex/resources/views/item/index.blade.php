@extends('layouts.app')

@section('title')
Our Products
@endsection

@section('content')
<!-- Banner -->	
<section id="home" name="home"></section>
<div id="headerwrap" style="height: 250px">	
	<div class="container">
		<div class="logo">
			<img src="{{ URL::asset('images/logo.png') }}"  style="width: 200px"  />
		</div>
		<div class="banner-info" style="padding: 0">
			<h1>ALFATEX</h1>
			<h3>textile | convection | garment</h3>
	    </div>
	</div> <!--/ .container -->
</div><!--/ #headerwrap -->
<!-- /Banner -->

@if(Auth::check())
				
				<!-- ACCORDION -->
							<div class="accordion ac" id="accordion2" align="center">
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"><u><b>Add</b> Item</u></a>
									</div>
									<div id="collapseTwo" class="accordion-body collapse">
										<div class="accordion-inner">
											 @include('item.create')
							</div><!-- /accordion-inner -->
						</div><!-- /collapse -->
						@include('flash::message')
					</div><!-- /accordion-group -->
		        </div><!-- Accordion -->
@endif
				
<!-- LOOPING 1 -->
@foreach($categories as $category)
<div id="intro" style="padding: 25px 0; background-color: #585858;">
<h3 class="text-center" style="padding: 0">
@if(Auth::check())
		 <a href="{{route('category.edit',$category->category_id)}}" title="Edit"><img src="{{URL::asset('images')}}/edit.png" width="17px"/></a> - 
@endif
{{ $category->category_name }}</h3>
</div>
<!-- Gallery -->
<section class="our-team" id="team" style="background-color: #fff; background-image: url('')">
	<div class="container">
		@foreach ($category->items as $item)
		<div class="col-sm-3" align="center">
		<a href="{{ URL::asset('img_item') }}/{{ $item->picture }}" title="{{ ucfirst($item->item_name) }}"><img src="{{ URL::asset('img_item') }}/{{ $item->picture }}" alt="{{ ucfirst($item->item_name) }}" class="img-responsive" style="height:220px"></img></a>
		@if(Auth::check())
		<p style="padding:0; padding-bottom: 20px; color: #000"><a href="{{route('product.edit',$item->item_id)}}" title="Edit"><img src="{{URL::asset('images')}}/edit.png" width="17px"/></a>
		@endif
			<b>{{ $item->item_name }}</b></p>
		</div>
		@endforeach 
    </div>	
</section>
<!-- /Gallery -->
@endforeach	
<!-- /LOOPING 1 -->
@endsection