<div class="container" style="padding-bottom: 50px">
<form class="form-horizontal" role="form" method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
{!! csrf_field() !!}

    <label class="col-md-4 control-label">Item Name :</label>

            <input type="text" class="form-control" name="item_name" value="{{ old('item_name') }}" style="width:40%">

            @if ($errors->has('item_name'))
                <span>
                    <strong>{{ $errors->first('item_name') }}</strong>
                </span>
            @endif

    <label class="col-md-4 control-label">Category :</label>

            <select id="category_id" name="category_id" class="form-control" style="width:40%">
					<option value="new">-- New Category..</option>
                @foreach($categories as $category)
                    <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                @endforeach
            </select>
			
			<label id="labelcat" class="col-md-4 control-label" style="display:none">New Category Name :</label>
			<input id="category_name" name="category_name" type="text" class="form-control" style="display:none; width:40%" placeholder="Input your new category name here.."/>
			
            @if ($errors->has('category_id'))
                <span>
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif

	<div class="form-group">
	<input type="file" name="file">
	</div>
            @if ($errors->has('file'))
                <span>
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
            @endif

    <button type="submit" class="btn btn-primary">Create Material</button>
</form>
</div>

@push('scripts')
<script type="text/javascript">
document.getElementById("category_id").selectedIndex = -1;
$('#category_id').change(function() {
    var val=$('#category_id').val();
    if(val=="new") {
        $('#category_name').show();
		$('#labelcat').show();
		}  else  {
      $('#category_name').hide();
      $('#labelcat').hide();
    }
});
</script>
@endpush