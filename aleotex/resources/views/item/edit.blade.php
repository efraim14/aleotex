@extends('layouts.app')

@section('title')
Our Products
@endsection

@section('content')
<section class="our-gallery" id="gallery" style="margin-bottom:100px">	
	<h3 class="text-center" style="padding-bottom: 15px">EDIT ITEM</h3>
	
		<form class="form-horizontal" method="POST" action="{{ route('product.destroy',$item->item_id) }}">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" value="Delete This Item">
    </form>
	<p class="text-center">Item Image :<br><img src="{{ URL::asset('img_item') }}/{{ $item->picture }}" width="200px"/></p>
	
	
	
<form class="form-horizontal" role="form" method="POST" action="{{ route('product.update',$item->item_id) }}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <label class="col-md-4 control-label">Item Name :</label>

            <input type="text" class="form-control" name="item_name" value="{{ old('item_name',$item->item_name) }}" style="width:38%">

            @if ($errors->has('item_name'))
                <span>
                    <strong>{{ $errors->first('item_name') }}</strong>
                </span>
            @endif

    <label class="col-md-4 control-label">Category :</label>

            <select name="category_id" class="form-control" style="width:38%">
                @foreach($categories as $category)
                    <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                @endforeach
            </select>

            @if ($errors->has('category_id'))
                <span>
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif

	<label class="col-md-4 control-label">Item Image :</label>
	<input type="file" name="file">
	
            @if ($errors->has('file'))
                <span>
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
            @endif

    <button type="submit" class="btn btn-primary">Update Item</button>
	<a href="{{ route('product.index') }}"><button type="button" class="btn btn-success">Go Back</button></a>
</form>
</section>
@endsection