@extends('layouts.app')

@section('title')
Our Clients
@endsection

@section('content')
<!-- Banner -->	
<section id="home" name="home"></section>
<div id="headerwrap" style="height: 250px">	
	<div class="container">
		<div class="logo">
			<img src="{{ URL::asset('images/logo.png') }}"  style="width: 200px"  />
		</div>
		<div class="banner-info" style="padding: 0">
			<h1>ALFATEX</h1>
			<h3>textile | convection | garment</h3>
	    </div>
	</div> <!--/ .container -->
</div><!--/ #headerwrap -->
<!-- /Banner -->

@if(Auth::check())
				
				<!-- ACCORDION -->
							<div class="accordion ac" id="accordion2" align="center">
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"><u><b>Add</b> Client</u></a>
									</div>
									<div id="collapseTwo" class="accordion-body collapse">
										<div class="accordion-inner">
											 @include('client.create')
							</div><!-- /accordion-inner -->
						</div><!-- /collapse -->
						@include('flash::message')
					</div><!-- /accordion-group -->
		        </div><!-- Accordion -->
@endif
				

<!-- Gallery -->
<section class="our-team" id="team" style="background-color: #fff; background-image: url('')">
<h3 class="text-center">We Proud to Served Them</h3>
<p class="text-center" style="color:#000">Our main goal is to keep our clients satisfied.</p>
	<div class="container">
		@foreach ($clients as $client)
		<div class="col-sm-4" align="center">
		<a href="{{ URL::asset('img_client') }}/{{ $client->picture }}" title="{{ ucfirst($client->client_name) }}"><img src="{{ URL::asset('img_client') }}/{{ $client->picture }}" alt="{{ ucfirst($client->client_name) }}" class="img-responsive" width="220px"></img></a>
		@if(Auth::check())
		<p style="padding:0; color: #000"><a href="{{route('client.edit',$client->client_id)}}" title="Edit"><img src="{{URL::asset('images')}}/edit.png" width="17px"/></a>
		@endif
		</div>
		@endforeach 
    </div>	
</section>
@endsection