@extends('layouts.app')

@section('title')
Our Clients
@endsection

@section('content')
<section class="our-gallery" id="gallery" style="margin-bottom:100px">	
	<h3 class="text-center" style="padding-bottom: 15px">EDIT ITEM</h3>
	
			<form class="form-horizontal" method="POST" action="{{ route('client.destroy',$client->client_id) }}">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" value="Delete This Client">
    </form>
	<p class="text-center">Client Image :<br><img src=" {{ URL::asset('img_client') }}/{{ $client->picture }}" width="200px"/></p>
	
<form class="form-horizontal" role="form" method="POST" action="{{ route('client.update',$client->client_id) }}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <label class="col-md-4 control-label">Client Name :</label>

            <input type="text" class="form-control" name="client_name" value="{{ old('client_name', $client->client_name) }}" style="width:40%">

            @if ($errors->has('client_name'))
                <span>
                    <strong>{{ $errors->first('client_name') }}</strong>
                </span>
            @endif

    <label class="col-md-4 control-label">Image File :</label>
    <input type="file" name="file" value="{{ old('picture', $client->picture) }}">
   
            @if ($errors->has('file'))
                <span>
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
            @endif

    <button type="submit" class="btn btn-primary">Update Client</button>
	<a href="{{ route('client.index') }}"><button type="button" class="btn btn-success">Go Back</button></a>
</form>
</section>
@endsection