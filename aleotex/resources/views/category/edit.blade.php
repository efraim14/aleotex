@extends('layouts.app')

@section('title')
Price List
@endsection

@section('content')
<section class="our-gallery" id="gallery" style="margin-bottom:100px">	
	<h3 class="text-center" style="padding-bottom: 15px">EDIT CATEGORY</h3>
	
			<form class="form-horizontal" method="POST" action="{{ route('category.destroy',$category->category_id) }}" style="padding-bottom:50px">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" value="Delete This Category">
    </form>
	
<form class="form-horizontal" role="form" method="POST" action="{{ route('category.update',$category->category_id) }}">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <label class="col-md-4 control-label">Category Name :</label>

            <input type="text" class="form-control" name="category_name" value="{{ old('category_name', $category->category_name) }}" style="width:40%">

            @if ($errors->has('category_name'))
                <span>
                    <strong>{{ $errors->first('category_name') }}</strong>
                </span>
            @endif

    <button type="submit" class="btn btn-primary">Update Category</button>
	<a href="{{ route('pricelist.index') }}"><button type="button" class="btn btn-success">Go Back</button></a>
</form>
</section>
@endsection