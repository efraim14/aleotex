<form class="form-horizontal" role="form" method="POST" action="{{ route('category.store') }}">
{!! csrf_field() !!}

    <label class="col-md-4 control-label">Category Name</label>

            <input type="text" class="form-control" name="category_name" value="{{ old('category_name') }}">

            @if ($errors->has('category_name'))
                <span>
                    <strong>{{ $errors->first('category_name') }}</strong>
                </span>
            @endif

    <button type="submit" class="btn btn-primary">Create Category</button>
</form>