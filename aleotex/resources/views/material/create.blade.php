<div class="container" style="padding-bottom: 50px">
<form class="form-horizontal" role="form" method="POST" action="{{ route('material.store') }}" enctype="multipart/form-data">
{!! csrf_field() !!}

    <label class="col-md-4 control-label">Material Name :</label>

            <input type="text" class="form-control" name="material_name" value="{{ old('material_name') }}" style="width:40%">

            @if ($errors->has('material_name'))
                <span>
                    <strong>{{ $errors->first('material_name') }}</strong>
                </span>
            @endif
	
	<label class="col-md-4 control-label">Image :</label>
	<input type="file" name="file">
	<br>
            @if ($errors->has('file'))
                <span>
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
            @endif

    <button type="submit" class="btn btn-primary">Create Material</button>
</form>
</div>