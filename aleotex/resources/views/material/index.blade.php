@extends('layouts.app')

@section('title')
Raw Materials
@endsection

@section('content')
<!-- Banner -->	
<section id="home" name="home"></section>
<div id="headerwrap" style="height: 250px">	
	<div class="container">
		<div class="logo">
			<img src="{{ URL::asset('images/logo.png') }}"  style="width: 200px"  />
		</div>
		<div class="banner-info" style="padding: 0">
			<h1>ALFATEX</h1>
			<h3>textile | convection | garment</h3>
	    </div>
	</div> <!--/ .container -->
</div><!--/ #headerwrap -->
<!-- /Banner -->

@if(Auth::check())
				
				<!-- ACCORDION -->
							<div class="accordion ac" id="accordion2" align="center">
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"><u><b>Add</b> Material</u></a>
									</div>
									<div id="collapseTwo" class="accordion-body collapse">
										<div class="accordion-inner">
											 @include('material.create')
							</div><!-- /accordion-inner -->
						</div><!-- /collapse -->
						@include('flash::message')
					</div><!-- /accordion-group -->
		        </div><!-- Accordion -->
				@endif
				
<!-- Gallery -->
<section class="our-gallery" id="gallery">	
	<h3 class="text-center">RAW MATERIALS</h3>
	<p class="text-center">WE PROUD TO USE LOCAL MATERIALS</p>

	<div class="container">
		<ul class="ch-grid">
			@foreach ($materials as $material)
			<li>
				<div class="ch-item" style="background-image: url('img_material/{{ $material->picture }}'); background-size: 100%">
					<div class="ch-info">
						<h4>{{ ucfirst($material->material_name) }}</h4>
						@if(Auth::check())
						<ul class="social-icons1">
							<li><a href="{{route('material.edit',$material->material_id)}}"><i class="fa fa-edit"></i>Edit</a></li>
						</ul>
						@endif
					</div>
				</div>
			</li>
			@endforeach
		</ul>				
	</div>	
</section>		
<!-- /Gallery -->	
@endsection