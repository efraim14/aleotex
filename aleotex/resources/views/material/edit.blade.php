@extends('layouts.app')

@section('title')
Raw Materials
@endsection

@section('content')
<section class="our-gallery" id="gallery" style="margin-bottom:100px">	
	<h3 class="text-center" style="padding-bottom: 15px">EDIT MATERIAL</h3>
	
		<form class="form-horizontal" method="POST" action="{{ route('material.destroy',$material->material_id) }}">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" value="Delete This Material">
    </form>
	<p class="text-center">Material Image :<br><img src="{{ URL::asset('img_material') }}/{{ $material->picture }}" width="200px"/></p>
	
<form class="form-horizontal" role="form" method="POST" action="{{ route('material.update',$material->material_id) }}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <label class="col-md-4 control-label">Material Name : </label>

            <input type="text" class="form-control" name="material_name" value="{{ old('material_name', $material->material_name) }}" style="width:38%">

            @if ($errors->has('material_name'))
                <span>
                    <strong>{{ $errors->first('material_name') }}</strong>
                </span>
            @endif

    <label class="col-md-4 control-label">Image File : </label>
    <input type="file" name="file" value="{{ old('picture', $material->picture) }}">
	
            @if ($errors->has('file'))
                <span>
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
            @endif
	<button type="submit" class="btn btn-primary">Update Material</button>
	<a href="{{ route('material.index') }}#gallery"><button type="button" class="btn btn-success">Go Back</button></a>
</form>
</section>
@endsection